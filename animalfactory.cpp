///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animalfactory.hpp
/// @version 1.0
///
/// Chooses random animal 
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   12_Feb_2021
///////////////////////////////////////////////////////////////////////////////


#include "animalfactory.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "aku.hpp"



namespace animalfarm {

Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;

   int randomNumber = rand() % 6;

   switch(randomNumber) {

      case 0: newAnimal = new Cat   ( Cat::getRandomName(),       Animal::getRandomColor(),  Animal::getRandomGender());      break;
      case 1: newAnimal = new Dog   ( Dog::getRandomName(),       Animal::getRandomColor(),  Animal::getRandomGender());      break;
      case 2: newAnimal = new Nunu  ( Animal::getRandomBool(),         Animal::getRandomColor(), Animal::getRandomGender() );    break;
      case 3: newAnimal = new Aku   ( Animal::getRandomWeight(1.5,9.8), Animal::getRandomColor(),  Animal::getRandomGender() );     break;
      case 4: newAnimal = new Palila( Animal::getRandomName(),       YELLOW,                 Animal::getRandomGender());   break;
      case 5: newAnimal = new Nene  ( Animal::getRandomName(),         BROWN,                  Animal::getRandomGender());     break;
   }

   return newAnimal;
}
}
