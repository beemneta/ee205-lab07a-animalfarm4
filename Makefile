###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author Beemnet Alemayehu <beemneta@hawaii.edu>
# @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
# @date   12_Feb_2021
###############################################################################
all: main 

animal.o: animal.cpp animal.hpp
	g++ -c animal.cpp

cat.o: cat.hpp cat.cpp
	g++ -c cat.cpp

mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp

fish.o: fish.cpp fish.hpp
	g++ -c fish.cpp

nunu.o: nunu.cpp nunu.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp
	g++ -c aku.cpp

bird.o: bird.cpp bird.hpp
	g++ -c bird.cpp

palila.o: palila.cpp palila.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp
	g++ -c nene.cpp

main.o: main.cpp animal.hpp animalfactory.cpp
	g++ -c main.cpp 

animalfactory.o: animalfactory.cpp *.hpp
	g++ -c animalfactory.cpp

node.o: node.hpp node.cpp
	g++ -c node.cpp

list.o: list.hpp list.cpp
	g++ -c list.cpp

main: *.hpp main.o animalfactory.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o 
	g++ -o main main.o animalfactory.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o 	

clean:
	rm -f *.o main
