///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   25_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include "list.hpp"
#include <cstdlib>
#include <iostream>


namespace animalfarm {
   
   Node* head = new Node();

   const bool SingleLinkedList::empty() const {
      
      return head == nullptr;

   }

   void SingleLinkedList::push_front( Node* newNode ){
      
      newNode->next = head;
      head = newNode;
   }

   Node* SingleLinkedList::pop_front(){
      
      Node* tmp = head;

      head = head->next;
      return tmp;


   }

   Node* SingleLinkedList::get_first() const{
      return head;
   }

   Node* SingleLinkedList::get_next( const Node* currentNode ) const {
      
      return currentNode->next;

   }

   unsigned int SingleLinkedList::size() const{

      unsigned int size = 0;
      
      Node* tmp = head;

      while(tmp!=nullptr){
         size=size + 1;
         tmp=tmp->next;
      
      }
      return size;
   }

}

