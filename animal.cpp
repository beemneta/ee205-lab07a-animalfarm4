///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   12_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

Animal::Animal(){
   cout<<".";
}

Animal::~Animal(){
   cout<<"X";
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

const enum Gender Animal::getRandomGender() {

   int randomNumber = rand() % 2; 
   
   switch (randomNumber) {
      case 0: return MALE;
      case 1: return FEMALE;
   }
   return MALE;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	
const enum Color Animal::getRandomColor(){
   
   int randomNumber = rand() % 6; 
   
   switch (randomNumber) {
      case 1: return SILVER;
      case 2: return YELLOW;
      case 3: return RED;
      case 4: return BROWN;
      case 5: return WHITE;
   }
   return BLACK;
}


string Animal::colorName (enum Color color) {
	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 1
   switch (color) {
      case BROWN: return string("Brown");break;
      case SILVER: return string("Silver");break;
      case RED: return string("Red");break;
      case YELLOW: return string("Yellow");break;
      case BLACK: return string("Black");break;
      case WHITE: return string("White");break;
 
   }
   return string("Unknown");
};

const bool Animal::getRandomBool(){
   int getRandomNumber = rand() % 2;

   switch (getRandomNumber) {
      case 0: return false;
   }
   return true;
}

const float Animal:: getRandomWeight(const float from, const float to) {
   int getRandomNumber = (rand() % int(to - from));

   float randomWeight = from + getRandomNumber;

   return randomWeight;
}

const string Animal::getRandomName() {
   int getRandomNumber = rand() % 6;

   int randomLength = getRandomNumber + 4;

   //ascii block letter 65 =A 90 = Z, lowercase a = 97,z =122
   
   int randomUpperCaseChar = rand() % 26 + 65 ;

   string randomName;

   randomName += char( randomUpperCaseChar );

   for (int i=1; i < randomLength; i++) {
      int randomLowerCaseChar = rand() % 26 + 97 ;

      randomName += char( randomLowerCaseChar );
   }


   return randomName;
}


} // namespace animalfarm
