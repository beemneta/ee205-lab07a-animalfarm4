///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   25_Mar_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

using namespace std;

namespace animalfarm {


class Node {
   protected:
      Node* next = nullptr;

   friend class SingleLinkedList;
};


}
