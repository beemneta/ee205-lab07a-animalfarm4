///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   12_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

   Nunu::Nunu( bool Native, enum Color newColor, enum Gender newGender) {

   species = "Fistularia chinensis";

   isNative = Native;
   
   gender = newGender;
   
   scaleColor = newColor;

   favoriteTemp = 80.6;

   
   }

void Nunu::printInfo() {
   cout << "Nunu" << endl;
   cout << "   Is native = [" << (isNative ? "true" : "false") << "]" << endl;
   Fish::printInfo();
}

}
